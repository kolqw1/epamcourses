package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Permission;
import com.epam.courses.WebProject.model.Person;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.courses.WebProject.filters.AttributeNames.PERSON;

/**
 * Если залогинился пользователь с правами ADMIN, то будет зарегистрирован пользователь с правами ADMIN, иначе права будут USER
 */
@WebServlet(name = "Registration", value="/Registration")
public class Registration extends HttpServlet {
    private PersonDao personDao;
    @Override
    public void init(ServletConfig config) throws ServletException {
        personDao=(PersonDao)config.getServletContext().getAttribute(DaoProvider.PERSON_DAO);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        Permission permission;
        HttpSession session=request.getSession();
        Person person= (Person) session.getAttribute(PERSON);
        if (person!=null && person.getPermission()==Permission.ADMIN)
            permission=Permission.ADMIN;
        else
            permission=Permission.USER;
        personDao.addPerson(new Person(request.getParameter("login"), request.getParameter("password"),
                request.getParameter("name"), request.getParameter("surname"), permission));
        response.sendRedirect("index.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

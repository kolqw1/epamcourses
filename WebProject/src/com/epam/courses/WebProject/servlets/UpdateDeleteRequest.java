package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.RequestDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Request;
import com.epam.courses.WebProject.model.RoomClass;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@WebServlet(name = "UpdateDeleteRequest", value="/UpdateDeleteRequest")
public class UpdateDeleteRequest extends HttpServlet {
    private RequestDao requestDao;
    private final static Logger log=Logger.getLogger(UpdateDeleteRooms.class);
    @Override
    public void init(ServletConfig config) throws ServletException {
        requestDao=(RequestDao)config.getServletContext().getAttribute(DaoProvider.REQUEST_DAO);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("utf-8");
            if (request.getParameter("delete") != null) {
                requestDao.deleteRequestById(Integer.parseInt(request.getParameter("id")));
            } else if (request.getParameter("update") != null) {
                requestDao.updateRoomAndCostById(Integer.parseInt(request.getParameter("id")), Integer.parseInt(request.getParameter("room")), Integer.parseInt(request.getParameter("cost")));
            }
            if (request.getParameter("from").equals("admin"))
                response.sendRedirect("admin.jsp");
            else if (request.getParameter("from").equals("user"))
                response.sendRedirect("user.jsp");
        } catch (Exception e){
            log.warn("Input error: "+e);
            if (request.getParameter("from").equals("admin"))
                response.sendRedirect("admin.jsp?error=true");
            else if (request.getParameter("from").equals("user"))
                response.sendRedirect("user.jsp?error=true");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.RoomDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Room;
import com.epam.courses.WebProject.model.RoomClass;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddRoom", value="/AddRoom")
public class AddRoom extends HttpServlet {
    private RoomDao roomDao;
    private final static Logger log=Logger.getLogger(AddRoom.class);
    @Override
    public void init(ServletConfig config) throws ServletException {
        roomDao=(RoomDao)config.getServletContext().getAttribute(DaoProvider.ROOM_DAO);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("utf-8");
            roomDao.addRoom(new Room(Integer.parseInt(request.getParameter("id")), Integer.parseInt(request.getParameter("numbeds")),
                    RoomClass.valueOf(request.getParameter("roomclass"))));
            response.sendRedirect("roomlist.jsp");
        } catch (Exception e) {
            log.warn("Input error: "+e);
            response.sendRedirect("roomlist.jsp?error=true");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

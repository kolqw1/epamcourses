package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Person;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Удаление пользователя. Самого себя удалить нельзя.
 */
@WebServlet(name = "DeletePerson", value="/DeletePerson")
public class DeletePerson extends HttpServlet {
    private PersonDao personDao;
    @Override
    public void init(ServletConfig config) throws ServletException {
        personDao=(PersonDao)config.getServletContext().getAttribute(DaoProvider.PERSON_DAO);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String login=request.getParameter("login");
        if (!login.equals(((Person)request.getSession().getAttribute("person")).getLogin()))
            personDao.deletePersonByLogin(request.getParameter("login"));
        response.sendRedirect("userlist.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

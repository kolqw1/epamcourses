package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.filters.AttributeNames;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Permission;
import com.epam.courses.WebProject.model.Person;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "UpdatePerson", value="/UpdatePerson")
public class UpdatePerson extends HttpServlet {
    private PersonDao personDao;
    @Override
    public void init(ServletConfig config) throws ServletException {
        personDao=(PersonDao)config.getServletContext().getAttribute(DaoProvider.PERSON_DAO);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        HttpSession session=request.getSession();
        Person person= (Person) session.getAttribute(AttributeNames.PERSON);
        if (request.getParameter("updatepassword")!=null)
            personDao.updatePasswordByLogin(person.getLogin(), request.getParameter("password"));
        else if(request.getParameter("updatename")!=null)
        {
            String name=request.getParameter("name");
            String surname=request.getParameter("surname");
            personDao.updateNameAndSurnameByLogin(person.getLogin(), name, surname);
            person.setName(name);
            person.setSurname(surname);
        }
        if (person.getPermission()== Permission.ADMIN)
            response.sendRedirect("admin.jsp");
        else if (person.getPermission()== Permission.USER)
            response.sendRedirect("user.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

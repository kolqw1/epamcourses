package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.RequestDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Person;
import com.epam.courses.WebProject.model.Request;
import com.epam.courses.WebProject.model.RoomClass;

import javax.ejb.Local;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import org.apache.log4j.Logger;

@WebServlet(name = "AddRequest", value="/AddRequest")
public class AddRequest extends HttpServlet {
    private RequestDao requestDao;
    private final static Logger log=Logger.getLogger(AddRequest.class);
    @Override
    public void init(ServletConfig config) throws ServletException {
        requestDao=(RequestDao)config.getServletContext().getAttribute(DaoProvider.REQUEST_DAO);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("utf-8");
            HttpSession session=request.getSession();
            Person person=(Person)session.getAttribute("person");
            requestDao.addRequest(new Request(Integer.parseInt(request.getParameter("numbeds")), RoomClass.valueOf(request.getParameter("roomclass")),
                    LocalDate.parse(request.getParameter("fromdate")), LocalDate.parse(request.getParameter("todate")), person.getLogin()));
            response.sendRedirect("user.jsp");
        } catch (Exception e) {
            log.warn("Input error: "+e);
            response.sendRedirect("user.jsp?error=true");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

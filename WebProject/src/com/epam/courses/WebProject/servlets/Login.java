package com.epam.courses.WebProject.servlets;

import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.hash.Hash;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Permission;
import com.epam.courses.WebProject.model.Person;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.courses.WebProject.filters.AttributeNames.PERSON;

@WebServlet(name = "Login", value="/Login")
public class Login extends HttpServlet {
    private PersonDao personDao;
    @Override
    public void init(ServletConfig config) throws ServletException {
        personDao=(PersonDao)config.getServletContext().getAttribute(DaoProvider.PERSON_DAO);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        Person person=personDao.getPersonByLogin(request.getParameter("login"));
        if (person!=null && person.getPassword().equals(Hash.md5(request.getParameter("password"))))
        {
            HttpSession session = request.getSession(true);
            if (session.getAttribute("local")==null)
                session.setAttribute("local", "ru");
            session.setAttribute(PERSON, person);
            if (person.getPermission() == Permission.USER)
                response.sendRedirect("user.jsp");
            else if (person.getPermission() == Permission.ADMIN)
                response.sendRedirect("admin.jsp");
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

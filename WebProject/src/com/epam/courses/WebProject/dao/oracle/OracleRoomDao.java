package com.epam.courses.WebProject.dao.oracle;

import com.epam.courses.WebProject.cp.ConnectionPool;
import com.epam.courses.WebProject.cp.ConnectionPoolException;
import com.epam.courses.WebProject.dao.interfaces.RoomDao;
import com.epam.courses.WebProject.model.Room;
import com.epam.courses.WebProject.model.RoomClass;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class OracleRoomDao implements RoomDao {
    private final static Logger log=Logger.getLogger(RoomDao.class);
    private ConnectionPool connectionPool;
    public OracleRoomDao(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }
    @Override
    public Collection<Room> getAll() {
        Collection<Room> result = new ArrayList<>();
        Connection connection = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.createStatement();
            rs = st.executeQuery("SELECT * FROM room ORDER BY id");
            while (rs.next()) {
                Room room = new Room(rs.getInt("id"), rs.getInt("numbeds"), RoomClass.values()[rs.getInt("roomclass")]);
                result.add(room);
            }
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st, rs);
        }
        return result;
    }

    @Override
    public Collection<Room> getRoomByNumBedsAndRoomClass(int numBeds, RoomClass roomClass) {
        Collection <Room> result= new ArrayList<>();
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("SELECT * FROM room WHERE numbeds=? AND roomclass=?  ORDER BY id");
            st.setInt(1, numBeds);
            st.setInt(2, roomClass.ordinal());
            rs = st.executeQuery();
            while (rs.next()) {
                Room room = new Room(rs.getInt("id"), rs.getInt("numbeds"), RoomClass.values()[rs.getInt("roomclass")]);
                result.add(room);
            }
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st, rs);
        }
        return result;
    }

    @Override
    public void deleteRoomById(int id) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("DELETE FROM room WHERE id=?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void updateRoomById(int id, Room room) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("UPDATE room SET id=?, numbeds=?, roomclass=? WHERE id=?");
            st.setInt(4, id);
            st.setInt(1, room.getId());
            st.setInt(2, room.getNumBeds());
            st.setInt(3, room.getRoomClass().ordinal());
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void addRoom(Room room) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("INSERT INTO room VALUES (?, ?, ?)");
            st.setInt(1, room.getId());
            st.setInt(2, room.getNumBeds());
            st.setInt(3, room.getRoomClass().ordinal());
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection!=null) connectionPool.closeConnection(connection, st);
        }
    }
}

package com.epam.courses.WebProject.dao.oracle;

import com.epam.courses.WebProject.cp.ConnectionPool;
import com.epam.courses.WebProject.cp.ConnectionPoolException;
import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.hash.Hash;
import com.epam.courses.WebProject.model.Permission;
import com.epam.courses.WebProject.model.Person;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class OraclePersonDao implements PersonDao {
    private final static Logger log=Logger.getLogger(PersonDao.class);
    private ConnectionPool connectionPool;
    public OraclePersonDao(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }
    @Override
    public Person getPersonByLogin(String login) {
        Person result = null;
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("SELECT * FROM person WHERE login=?");
            st.setString(1, login);
            rs = st.executeQuery();
            if (rs.next()) {
                result = new Person(rs.getString("login"), rs.getString("password"), rs.getString("name"), rs.getString("surname"), Permission.values()[rs.getInt("permission")]);
            }
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st, rs);
        }
        return result;
    }

    @Override
    public void addPerson(Person person) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("INSERT INTO person VALUES (?, ?, ?, ?, ?)");
            st.setString(1, person.getLogin());
            st.setString(2, Hash.md5(person.getPassword()));
            st.setString(3, person.getName());
            st.setString(4, person.getSurname());
            st.setInt(5, person.getPermission().ordinal());
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void updatePasswordByLogin(String login, String password) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("UPDATE person SET password=? WHERE login=?");
            st.setString(2, login);
            st.setString(1, Hash.md5(password));
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void updateNameAndSurnameByLogin(String login, String name, String surname) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("UPDATE person SET name=?, surname=? WHERE login=?");
            st.setString(3, login);
            st.setString(1, name);
            st.setString(2, surname);
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void deletePersonByLogin(String login) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("DELETE FROM person WHERE login=?");
            st.setString(1, login);
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public Collection<Person> getAll() {
        Collection<Person> result = new ArrayList<>();
        Connection connection = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.createStatement();
            rs = st.executeQuery("SELECT * FROM person ORDER BY login");
            while (rs.next()) {
                Person person = new Person(rs.getString("login"), rs.getString("password"), rs.getString("name"), rs.getString("surname"), Permission.values()[rs.getInt("permission")]);
                result.add(person);
            }
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st, rs);
        }
        return result;
    }
}

package com.epam.courses.WebProject.dao.oracle;

import com.epam.courses.WebProject.cp.ConnectionPool;
import com.epam.courses.WebProject.cp.ConnectionPoolException;
import com.epam.courses.WebProject.dao.interfaces.RequestDao;
import com.epam.courses.WebProject.model.Person;
import com.epam.courses.WebProject.model.Request;
import com.epam.courses.WebProject.model.RoomClass;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

public class OracleRequestDao implements RequestDao {
    private final static Logger log=Logger.getLogger(RequestDao.class);
    private ConnectionPool connectionPool;
    public OracleRequestDao(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }
    @Override
    public Collection<Request> getAll() {
        Collection <Request> result= new ArrayList<>();
        Connection connection = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.createStatement();
            rs = st.executeQuery("SELECT * FROM request ORDER BY fromdate");
            while (rs.next()) {
                Request request = new Request(rs.getInt("id"), rs.getInt("numbeds"), RoomClass.values()[rs.getInt("roomclass")],
                        rs.getDate("fromdate").toLocalDate(), rs.getDate("todate").toLocalDate(), rs.getInt("cost"), rs.getString("login"), rs.getInt("room"));
                result.add(request);
            }
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st, rs);
        }
        return result;
    }

    @Override
    public Collection<Request> getRequestsByPerson(Person person) {
        Collection <Request> result= new ArrayList<>();
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("SELECT * FROM request WHERE login=?  ORDER BY fromdate");
            st.setString(1, person.getLogin());
            rs = st.executeQuery();
            while (rs.next()) {
                Request request = new Request(rs.getInt("id"), rs.getInt("numbeds"), RoomClass.values()[rs.getInt("roomclass")],
                        rs.getDate("fromdate").toLocalDate(), rs.getDate("todate").toLocalDate(), rs.getInt("cost"), rs.getString("login"), rs.getInt("room"));
                result.add(request);
            }
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st, rs);
        }
        return result;
    }

    @Override
    public void updateRoomAndCostById(int id, int room, int cost) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("UPDATE request SET room=?, cost=? WHERE id=?");
            st.setInt(3, id);
            st.setInt(1, room);
            st.setInt(2, cost);
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void deleteRequestById(int id) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("DELETE FROM request WHERE id=?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection != null) connectionPool.closeConnection(connection, st);
        }
    }

    @Override
    public void addRequest(Request request) {
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = connectionPool.takeConnection();
            st = connection.prepareStatement("INSERT INTO request (numbeds, roomclass, fromdate, todate, login) VALUES (?, ?, ?, ?, ?)");
            st.setInt(1, request.getNumBeds());
            st.setInt(2, request.getRoomClass().ordinal());
            st.setDate(3, Date.valueOf(request.getFromDate()));
            st.setDate(4, Date.valueOf(request.getToDate()));
            st.setString(5, request.getLogin());
            st.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            log.error(e);
        } finally {
            if (connection!=null) connectionPool.closeConnection(connection, st);
        }
    }
}

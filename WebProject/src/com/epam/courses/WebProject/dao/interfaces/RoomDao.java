package com.epam.courses.WebProject.dao.interfaces;

import com.epam.courses.WebProject.model.Room;
import com.epam.courses.WebProject.model.RoomClass;

import java.util.Collection;

public interface RoomDao {
    Collection<Room> getAll();
    Collection<Room> getRoomByNumBedsAndRoomClass(int numBeds, RoomClass roomClass);
    void deleteRoomById(int id);
    void updateRoomById(int id, Room room);
    void addRoom(Room room);
}

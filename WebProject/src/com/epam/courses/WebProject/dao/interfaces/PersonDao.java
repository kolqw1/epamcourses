package com.epam.courses.WebProject.dao.interfaces;

import com.epam.courses.WebProject.model.Person;

import java.util.Collection;

public interface PersonDao {
    /**
     * Получение пользователя по логину
     * @param login Логин
     * @return Пользователь
     */
    Person getPersonByLogin(String login);
    void addPerson(Person person);
    void updatePasswordByLogin(String login, String password);
    void updateNameAndSurnameByLogin(String login, String name, String surname);
    void deletePersonByLogin(String login);
    Collection<Person> getAll();
}

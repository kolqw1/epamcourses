package com.epam.courses.WebProject.dao.interfaces;

import com.epam.courses.WebProject.model.Person;
import com.epam.courses.WebProject.model.Request;

import java.util.Collection;

public interface RequestDao {
    Collection<Request> getAll();
    Collection<Request> getRequestsByPerson(Person person);
    void updateRoomAndCostById(int id, int room, int cost);
    void deleteRequestById(int id);
    void addRequest(Request request);
}

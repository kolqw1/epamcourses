package com.epam.courses.WebProject.filters;

import com.epam.courses.WebProject.model.Permission;
import com.epam.courses.WebProject.model.Person;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.courses.WebProject.filters.AttributeNames.PERSON;

/**
 * Доступ к перечисленным страницам будет осуществляться только, если пользователь залогинился и у него права ADMIN
 */
@WebFilter(filterName = "AdminFilter", value = {"/admin.jsp", "/roomlist.jsp", "/userlist.jsp", "/AddRoom",
        "/DeletePerson", "/UpdateDeleteRooms"})
public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpSession session=((HttpServletRequest)req).getSession();
        Person person=(Person)session.getAttribute(PERSON);
        if (person==null || person.getPermission()!= Permission.ADMIN)
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        else
            chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

package com.epam.courses.WebProject.model;

public class Person {
    private String login;
    private String password;
    private String name;
    private String surname;
    private Permission permission;

    public Person(String login, String password, String name, String surname, Permission permission) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.permission = permission;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}

package com.epam.courses.WebProject.model;


public class Room {
    private int id;
    private int numBeds;
    private RoomClass roomClass;

    public Room(int id, int numBeds, RoomClass roomClass) {
        this.id = id;
        this.numBeds = numBeds;
        this.roomClass = roomClass;
    }

    public int getId() {
        return id;
    }

    public int getNumBeds() {
        return numBeds;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }
}

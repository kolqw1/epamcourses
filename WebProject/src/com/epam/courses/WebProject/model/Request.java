package com.epam.courses.WebProject.model;

import java.time.LocalDate;
import java.util.Date;
/**
 *roomId и personLogin - foreign keys, roomId может быть null
 */
public class Request {
    private int id;
    private int numBeds;
    private RoomClass roomClass;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String personLogin;
    private int roomId;
    private int cost;

    public Request(int numBeds, RoomClass roomClass, LocalDate fromDate, LocalDate toDate, String login) {
        this.numBeds = numBeds;
        this.roomClass = roomClass;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.personLogin = login;
    }

    public Request(int id, int numBeds, RoomClass roomClass, LocalDate fromDate, LocalDate toDate, int cost, String login, int room) {
        this.id = id;
        this.numBeds = numBeds;
        this.roomClass = roomClass;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.personLogin = login;
        this.roomId = room;
        this.cost = cost;
    }
    public int getNumBeds() {
        return numBeds;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public String getLogin() {
        return personLogin;
    }

    public int getRoom() {
        return roomId;
    }

    public int getCost() {
        return cost;
    }

    public int getId() {
        return id;
    }
}

package com.epam.courses.WebProject.taglibs;

import com.epam.courses.WebProject.dao.interfaces.RoomDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Room;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Генерация формы для просмотра и редактирования номеров. Для каждого номера-своя форма.
 */
public class RoomListTagLib extends TagSupport {
    private final static Logger log=Logger.getLogger(RoomListTagLib.class);
    @Override
    public int doStartTag() throws JspException {
        try {
            RoomDao roomDao=(RoomDao) pageContext.getServletContext().getAttribute(DaoProvider.ROOM_DAO);
            Collection<Room> rooms=roomDao.getAll();
            JspWriter writer=pageContext.getOut();
            ResourceBundle bundle=ResourceBundle.getBundle("com.epam.courses.WebProject.resources/locale",
                    new Locale((String)pageContext.getSession().getAttribute("local")));
            for (Room i:rooms) {
                writer.write("<form action='UpdateDeleteRooms' method='post'>");
                writer.write("<input type='hidden' name='id2' value='"+i.getId()+"'>");
                writer.write("<p> <label>" +"Id "+ "<input type='number' name='id' value='"+i.getId()+"'> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.numbeds")+ " <input type='number' name='numbeds' value="+i.getNumBeds()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.roomclass")+  " <input type='text' name='roomclass2' disabled=true value='"+i.getRoomClass()+"'> </label> </p>");
                writer.write("<p> <label>"+bundle.getString("local.user.roomclass")+"<input type='radio' name='roomclass' value='STANDARD'>"+bundle.getString("local.user.standard"));
                writer.write("<input type='radio' name='roomclass' value='BUSINESS'>"+bundle.getString("local.user.business"));
                writer.write("<input type='radio' name='roomclass' value='LUX'>"+bundle.getString("local.user.lux")+"</label> </p>");
                writer.write("<p> <input type='submit' name='update' value='"+bundle.getString("local.change")+"'> </p>");
                writer.write("<p> <input type='submit' name='delete' value='"+bundle.getString("local.delete")+"'> </p>");
                writer.write("</form>");
            }
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}

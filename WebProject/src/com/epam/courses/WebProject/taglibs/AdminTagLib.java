package com.epam.courses.WebProject.taglibs;

import com.epam.courses.WebProject.dao.interfaces.RequestDao;
import com.epam.courses.WebProject.dao.interfaces.RoomDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Request;
import com.epam.courses.WebProject.model.Room;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.*;

/**
 * Генерация формы для управления заявками пользователей администратором. Для каждой заявки - своя форма.
 * Для каждой заявки подбирается список номеров, подходящих под требованиям  к классу и количеству мест.
 */
public class AdminTagLib extends TagSupport {
    private final static Logger log=Logger.getLogger(AdminTagLib.class);
    @Override
    public int doStartTag() throws JspException {
        try {
            RequestDao requestDao=(RequestDao) pageContext.getServletContext().getAttribute(DaoProvider.REQUEST_DAO);
            Collection<Request> requests=requestDao.getAll();
            RoomDao roomDao=(RoomDao) pageContext.getServletContext().getAttribute(DaoProvider.ROOM_DAO);
            JspWriter writer=pageContext.getOut();
            ResourceBundle bundle=ResourceBundle.getBundle("com.epam.courses.WebProject.resources/locale",
                    new Locale((String)pageContext.getSession().getAttribute("local")));
            for (Request i:requests) {
                writer.write("<form action='UpdateDeleteRequest' method='post'>");
                writer.write("<input type='hidden' name='from' value='admin'>");
                writer.write("<input type='hidden' name='id' value='"+i.getId()+"'>");
                writer.write("<p> <label>" +bundle.getString("local.user.numbeds")+ " <input type='number' name='numbeds' disabled=true value="+i.getNumBeds()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.roomclass")+  " <input type='text' name='roomclass' disabled=true value='"+i.getRoomClass()+"'> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.fromdate")+  " <input type='date' name='fromdate' disabled=true value="+i.getFromDate()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.todate")+  " <input type='date' name='todate' disabled=true value="+i.getToDate()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.admin.login")+  " <input type='text' name='login' disabled=true value="+i.getLogin()+"> </label> </p>");
                //writer.write("<p> <label>" +bundle.getString("local.admin.room")+ " <input type='number' name='room' value="+i.getRoom()+"> </label> </p>");
                Collection<Room> rooms=roomDao.getRoomByNumBedsAndRoomClass(i.getNumBeds(), i.getRoomClass());
                writer.write("<p><select size=1 name='room'>");
                writer.write("<option>"+bundle.getString("local.admin.room")+"</option>");
                for (Room j:rooms)
                    if (j.getId()==i.getRoom())
                        writer.write("<option selected value="+j.getId()+">"+j.getId()+"</option>");
                    else
                        writer.write("<option value="+j.getId()+">"+j.getId()+"</option>");
                writer.write("</select></p>");
                writer.write("<p> <label>" +bundle.getString("local.admin.cost")+ " <input type='number' name='cost' value="+i.getCost()+"> </label> </p>");
                writer.write("<p> <input type='submit' name='update' value='"+bundle.getString("local.admin.updatebutton")+"'> </p>");
                writer.write("<p> <input type='submit' name='delete' value='"+bundle.getString("local.user.deletebutton")+"'> </p>");
                writer.write("</form>");
            }
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}

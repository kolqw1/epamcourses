package com.epam.courses.WebProject.taglibs;

import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Person;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Генерация формы для просмотра и редактирования пользователей. Для каждого пользователя-своя форма.
 */
public class UserListTagLib extends TagSupport {
    private final static Logger log=Logger.getLogger(UserListTagLib.class);
    @Override
    public int doStartTag() throws JspException {
        try {
            PersonDao personDao=(PersonDao) pageContext.getServletContext().getAttribute(DaoProvider.PERSON_DAO);
            Collection<Person> persons=personDao.getAll();
            JspWriter writer=pageContext.getOut();
            ResourceBundle bundle=ResourceBundle.getBundle("com.epam.courses.WebProject.resources/locale",
                    new Locale((String)pageContext.getSession().getAttribute("local")));
            for (Person i:persons) {
                writer.write("<form action='DeletePerson' method='post'>");
                writer.write("<input type='hidden' name='login' value='"+i.getLogin()+"'>");
                writer.write("<p> <label>" +bundle.getString("local.login.login")+ " <input type='text' name='login2' disabled=true value="+i.getLogin()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.registration.name")+  " <input type='text' name='name' disabled=true value='"+i.getName()+"'> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.registration.surname")+  " <input type='text' name='surname' disabled=true value="+i.getSurname()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.registration.permission")+  " <input type='text' name='permission' disabled=true value="+i.getPermission()+"> </label> </p>");
                //writer.write("<p> <input type='submit' name='update' value='"+bundle.getString("local.change")+"'> </p>");
                writer.write("<p> <input type='submit' name='delete' value='"+bundle.getString("local.delete")+"'> </p>");
                writer.write("</form>");
            }
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}

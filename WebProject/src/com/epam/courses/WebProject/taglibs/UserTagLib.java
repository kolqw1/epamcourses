package com.epam.courses.WebProject.taglibs;

import com.epam.courses.WebProject.dao.interfaces.RequestDao;
import com.epam.courses.WebProject.listeners.DaoProvider;
import com.epam.courses.WebProject.model.Person;
import com.epam.courses.WebProject.model.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Генерация формы для просмотра заявок от пользователя, который залогинился. Для каждой заявки-своя форма.
 * Редактировать запрещено, т.к. тогда можно было бы редактировать после выставления счета.
 */
public class UserTagLib extends TagSupport {
    private final static Logger log=Logger.getLogger(UserTagLib.class);
    @Override
    public int doStartTag() throws JspException {
        try {
            RequestDao requestDao=(RequestDao) pageContext.getServletContext().getAttribute(DaoProvider.REQUEST_DAO);
            HttpSession session=pageContext.getSession();
            Collection<Request> requests=requestDao.getRequestsByPerson((Person)session.getAttribute("person"));
            JspWriter writer=pageContext.getOut();
            ResourceBundle bundle=ResourceBundle.getBundle("com.epam.courses.WebProject.resources/locale",
                    new Locale((String)session.getAttribute("local")));
            for (Request i:requests) {
                writer.write("<form action='UpdateDeleteRequest' method='post'>");
                writer.write("<input type='hidden' name='from' value='user'>");
                writer.write("<input type='hidden' name='id' value='"+i.getId()+"'>");
                writer.write("<p> <label>" +bundle.getString("local.user.numbeds")+ " <input type='number' disabled=true name='numbeds' value="+i.getNumBeds()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.roomclass")+  " <input type='text' disabled=true name='roomclass' value="+i.getRoomClass()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.fromdate")+  " <input type='date' disabled=true name='fromdate' value="+i.getFromDate()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.user.todate")+  " <input type='date' disabled=true name='todate' value="+i.getToDate()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.admin.room")+ " <input type='number' disabled=true name='room' value="+i.getRoom()+"> </label> </p>");
                writer.write("<p> <label>" +bundle.getString("local.admin.cost")+ " <input type='number' disabled=true name='cost' value="+i.getCost()+"> </label> </p>");
                //writer.write("<p> <input type='submit' name='update' value='"+bundle.getString("local.user.updatebutton")+"'/> </p>");
                writer.write("<p> <input type='submit' name='delete' value='"+bundle.getString("local.user.deletebutton")+"'/> </p>");
                writer.write("</form>");
            }
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}

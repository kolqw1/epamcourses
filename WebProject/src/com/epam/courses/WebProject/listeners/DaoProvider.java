package com.epam.courses.WebProject.listeners;

import com.epam.courses.WebProject.cp.ConnectionPool;
import com.epam.courses.WebProject.cp.ConnectionPoolException;
import com.epam.courses.WebProject.dao.interfaces.PersonDao;
import com.epam.courses.WebProject.dao.interfaces.RequestDao;
import com.epam.courses.WebProject.dao.interfaces.RoomDao;
import com.epam.courses.WebProject.dao.oracle.OraclePersonDao;
import com.epam.courses.WebProject.dao.oracle.OracleRequestDao;
import com.epam.courses.WebProject.dao.oracle.OracleRoomDao;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener()
public class DaoProvider implements ServletContextListener
{
    public static final String PERSON_DAO = "personDao";
    public static final String REQUEST_DAO = "requestDao";
    public static final String ROOM_DAO = "roomDao";
    private static ConnectionPool connectionPool;
    private final static Logger log=Logger.getLogger(DaoProvider.class);
    public void contextInitialized(ServletContextEvent sce) {
        final ServletContext servletContext = sce.getServletContext();

        connectionPool=new ConnectionPool();
        try {
            connectionPool.initPoolData();
        } catch (ConnectionPoolException e) {
            log.error(e);
        }
        final PersonDao personDao = new OraclePersonDao(connectionPool);
        final RequestDao requestDao = new OracleRequestDao(connectionPool);
        final RoomDao roomDao = new OracleRoomDao(connectionPool);
        servletContext.setAttribute(PERSON_DAO, personDao);
        servletContext.setAttribute(REQUEST_DAO, requestDao);
        servletContext.setAttribute(ROOM_DAO, roomDao);
    }

    public void contextDestroyed(ServletContextEvent sce) {
        connectionPool.dispose();
    }

}

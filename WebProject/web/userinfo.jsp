<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value='<%=request.getSession().getAttribute("local")%>'/>
<fmt:setBundle basename="com.epam.courses.WebProject.resources/locale" var="loc"/>
<fmt:message bundle="${loc}" key="local.userinfo.title" var="title"/>
<fmt:message bundle="${loc}" key="local.login.password" var="password"/>
<fmt:message bundle="${loc}" key="local.change" var="button"/>
<fmt:message bundle="${loc}" key="local.registration.name" var="name"/>
<fmt:message bundle="${loc}" key="local.registration.surname" var="surname"/>

<html>
<head>
    <title>${title}</title>
</head>
<body>
    <form action="UpdatePerson" method="post">
        <p> <label> ${password}  <input type="text" name="password"> </label> </p>
        <p> <input type="submit" name='updatepassword' value="${button}"/> </p>
    </form>
    <form action="UpdatePerson" method="post">
        <p> <label> ${name}  <input type="text" name="name" value=${sessionScope.person.name}> </label> </p>
        <p> <label> ${surname}  <input type="text" name="surname" value=${sessionScope.person.surname}> </label> </p>
        <p> <input type="submit" name='updatename' value="${button}"/> </p>
    </form>
</body>
</html>

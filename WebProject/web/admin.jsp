<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://epam.com/myTLD" prefix="mytag"%>

<fmt:setLocale value='<%=request.getSession().getAttribute("local")%>'/>
<fmt:setBundle basename="com.epam.courses.WebProject.resources/locale" var="loc"/>
<fmt:message bundle="${loc}" key="local.admin.title" var="title"/>

<fmt:message bundle="${loc}" key="local.user.logout" var="logout"/>
<fmt:message bundle="${loc}" key="local.registration.title" var="register"/>
<fmt:message bundle="${loc}" key="local.userlist.title" var="userlist"/>
<fmt:message bundle="${loc}" key="local.roomlist.title" var="roomlist"/>
<fmt:message bundle="${loc}" key="local.userinfo.title" var="userinfo"/>

<html>
<head>
    <title>${title}</title>
</head>
<body>
    <mytag:adminTagLib/>
    <%=request.getParameter("error")==null?"":"Error!"%>
    <div align="center">
        <a href="userinfo.jsp"> ${userinfo}</a> <br>
        <a href="userlist.jsp"> ${userlist}</a> <br>
        <a href="roomlist.jsp"> ${roomlist}</a> <br>
        <a href="registration.jsp"> ${register}</a> <br>
    </div>
    <br>
    <div align="center">
        <form action="Logout" method="post">
            <input type="submit" value="${logout}" />
        </form>
    </div>
</body>
</html>

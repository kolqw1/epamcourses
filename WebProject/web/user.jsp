<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://epam.com/myTLD" prefix="mytag"%>

<fmt:setLocale value='<%=request.getSession().getAttribute("local")%>'/>
<fmt:setBundle basename="com.epam.courses.WebProject.resources/locale" var="loc"/>
<fmt:message bundle="${loc}" key="local.user.numbeds" var="numbeds"/>
<fmt:message bundle="${loc}" key="local.user.roomclass" var="roomclass"/>
<fmt:message bundle="${loc}" key="local.user.fromdate" var="fromdate"/>
<fmt:message bundle="${loc}" key="local.user.todate" var="todate"/>
<fmt:message bundle="${loc}" key="local.user.insertbutton" var="button"/>
<fmt:message bundle="${loc}" key="local.user.title" var="title"/>
<fmt:message bundle="${loc}" key="local.user.logout" var="logout"/>
<fmt:message bundle="${loc}" key="local.user.standard" var="standard"/>
<fmt:message bundle="${loc}" key="local.user.business" var="business"/>
<fmt:message bundle="${loc}" key="local.user.lux" var="lux"/>
<fmt:message bundle="${loc}" key="local.userinfo.title" var="userinfo"/>

<html>
<head>
    <title>${title}</title>
</head>
<body>
    <mytag:userTagLib/>
    <form action="AddRequest" method="post">
        <p> <label> ${numbeds} <input type="number" name="numbeds"> </label> </p>
        <p> <label> ${roomclass}  <input type="radio" name="roomclass" value="STANDARD"> ${standard}
            <input type="radio" name="roomclass" value="BUSINESS"> ${business}
            <input type="radio" name="roomclass" value="LUX"> ${lux}</label> </p>
        <p> <label> ${fromdate}  <input type="date" name="fromdate"> </label> </p>
        <p> <label> ${todate}  <input type="date" name="todate"> </label> </p>
        <p> <input type="submit" value="${button}"/> </p>
    </form>
    <%=request.getParameter("error")==null?"":"Error!"%>
    <div align="center">
        <a href="userinfo.jsp"> ${userinfo}</a> <br> <br>
        <form action="Logout" method="post">
            <input type="submit" value="${logout}" />
        </form>
    </div>
</body>
</html>

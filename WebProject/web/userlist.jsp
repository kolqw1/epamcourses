<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://epam.com/myTLD" prefix="mytag"%>

<fmt:setLocale value='<%=request.getSession().getAttribute("local")%>'/>
<fmt:setBundle basename="com.epam.courses.WebProject.resources/locale" var="loc"/>
<fmt:message bundle="${loc}" key="local.userlist.title" var="title"/>
<fmt:message bundle="${loc}" key="local.back" var="back"/>

<html>
<head>
    <title>${title}</title>
</head>
<body>
    <mytag:userListTagLib/>
    <div align="center">
        <a href="admin.jsp"> ${back}</a>
    </div>
</body>
</html>

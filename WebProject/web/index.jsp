<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%--
<fmt:setLocale value='<%=request.getParameter("local")%>'/>
<fmt:setLocale value='<%=request.getSession().getAttribute("local")%>'/>
<%
  String param=request.getParameter("local");
  if (param!=null)
    session.setAttribute("local", param); %>
--%>

<c:set var="local" scope="session" value='<%=request.getParameter("local")!=null?request.getParameter("local"):session.getAttribute("local")%>'/>

<fmt:setLocale value="${sessionScope.local}"/>
<fmt:setBundle basename="com.epam.courses.WebProject.resources/locale" var="loc"/>
<fmt:message bundle="${loc}" key="local.login.login" var="login"/>
<fmt:message bundle="${loc}" key="local.login.password" var="password"/>
<fmt:message bundle="${loc}" key="local.login.button" var="button"/>
<fmt:message bundle="${loc}" key="local.login.title" var="title"/>
<fmt:message bundle="${loc}" key="local.registration.title" var="register"/>

<html>
  <head>
    <title>${title}</title>
  </head>
  <body>
    <div align="right">
      <form action="" method="get">
        <input type="hidden" name="local" value="ru" />
        <input type="submit" value="RU" />
      </form> 
      <form action="" method="get">
        <input type="hidden" name="local" value="en" />
        <input type="submit" value="EN" />
      </form>
    </div>

    <br><br><br><br><br><br><br>
    <div align="center">
      <form action="Login" method="post">
        <p> <label> ${login} <input type="text" name="login"> </label> </p>
        <p> <label> ${password}  <input type="password" name="password" autocomplete="off"> </label> </p>
        <p> <input type="submit" value="${button}"/> </p>
      </form>

      <a href="registration.jsp"> ${register}</a>
    </div>
  </body>
</html>

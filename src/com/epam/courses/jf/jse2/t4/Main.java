package com.epam.courses.jf.jse2.t4;

import com.epam.courses.jf.jse2.t3.*;

import java.util.ArrayList;
import java.util.Comparator;

class StationeryByPriceComparator implements Comparator<Stationery>
{
    @Override
    public int compare(Stationery o1, Stationery o2) {
        return (int)(o1.getPrice()-o2.getPrice());
    }
}

class StationeryByNameComparator implements Comparator<Stationery>
{
    @Override
    public int compare(Stationery o1, Stationery o2) {
        return o1.getClass().getSimpleName().compareTo(o2.getClass().getSimpleName());
    }
}

class StationeryByPriceAndNameComparator implements Comparator<Stationery>
{
    @Override
    public int compare(Stationery o1, Stationery o2) {
        if (o1.getPrice()>o2.getPrice())
            return 1;
        else if (o1.getPrice()<o2.getPrice())
            return -1;
        else{
            return o1.getClass().getSimpleName().compareTo(o2.getClass().getSimpleName());
        }
    }
}

public class Main {
    public static void main(String[] args) {
        ArrayList<Stationery> mass=new ArrayList<>();
        mass.add(new Note());
        mass.add(new Note());
        mass.add(new Pen());
        mass.add(new Pencil());
        mass.add(new Marker());
        mass.add(new Paper());
        System.out.print("Without sort          : ");
        for (Stationery i:mass ) {
            System.out.print(i.getClass().getSimpleName()+" ");
        }
        System.out.println();
        mass.sort(new StationeryByPriceComparator());
        System.out.print("Sort by price         : ");
        for (Stationery i:mass ) {
            System.out.print(i.getClass().getSimpleName()+" ");
        }
        System.out.println();
        mass.sort(new StationeryByNameComparator());
        System.out.print("Sort by name          : ");
        for (Stationery i:mass ) {
            System.out.print(i.getClass().getSimpleName()+" ");
        }
        System.out.println();
        mass.sort(new StationeryByPriceAndNameComparator());
        System.out.print("Sort by price and name: ");
        for (Stationery i:mass ) {
            System.out.print(i.getClass().getSimpleName()+" ");
        }
    }
}

package com.epam.courses.jf.jse2.t1;

enum Color
{
    BLUE,
    RED,
    BLACK,
    GREEN
}

public class Pen {
    private int ink;//кол-во чернил
    private double length;
    private Color color;
    Pen(double length)
    {
        this.length=length;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pen pen = (Pen) o;

        if (Double.compare(pen.ink, ink) != 0) return false;
        if (Double.compare(pen.length, length) != 0) return false;
        return color == pen.color;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(ink);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(length);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + color.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Pen{" +
                "ink=" + ink +
                ", length=" + length +
                ", color=" + color +
                '}';
    }

    public void setInkAndColor(int ink, Color color) {
        this.ink = ink;
        this.color = color;
    }
    public int getInk() {
        return ink;
    }
    public double getLength() {
        return length;
    }
    public Color getColor() {
        return color;
    }
    public void writeString(String string)
    {
        if (ink>=string.length())
        {
            System.out.println(string);
            ink -= string.length();
        }
    }
}

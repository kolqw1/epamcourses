package com.epam.courses.jf.jse2.t5;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SubjectStudentTest {
    @Test
    public void Test()
    {
        ArrayList <Student> students=new ArrayList<>();
        ArrayList<SubjectStudent> subjectStudents=new ArrayList<>();
        for (int i=0; i<5; i++)
            students.add(new Student());
        subjectStudents.add(new SubjectStudent(students.get(0), Subject.MATHEMATICAL_ANALYSIS, 1));
        subjectStudents.add(new SubjectStudent(students.get(0), Subject.PHYSICS, 2));
        subjectStudents.add(new SubjectStudent(students.get(1), Subject.COMPUTING_MATHEMATICS, 3));
        subjectStudents.add(new SubjectStudent(students.get(1), Subject.PROGRAMMING, 4));
        subjectStudents.add(new SubjectStudent(students.get(2), Subject.THEORY_OF_PROBABILITY, 5));
        subjectStudents.add(new SubjectStudent(students.get(2), Subject.MATHEMATICAL_ANALYSIS, 2));
        subjectStudents.add(new SubjectStudent(students.get(3), Subject.PHYSICS, 3));
        subjectStudents.add(new SubjectStudent(students.get(3), Subject.COMPUTING_MATHEMATICS, 4));
        subjectStudents.add(new SubjectStudent(students.get(4), Subject.PROGRAMMING, 2.1));
        subjectStudents.add(new SubjectStudent(students.get(4), Subject.MATHEMATICAL_ANALYSIS, 3.5));
        subjectStudents.add(new SubjectStudent(students.get(4), Subject.PHYSICS, 3.2));
        for (SubjectStudent i:subjectStudents)
        {
            if (i.isStudent(students.get(4)))
            {
                System.out.println(i.getSubject()+" "+i.getMark());
            }
        }
    }
}
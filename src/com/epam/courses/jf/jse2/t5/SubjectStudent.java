package com.epam.courses.jf.jse2.t5;

enum Subject {
    MATHEMATICAL_ANALYSIS, PHYSICS, COMPUTING_MATHEMATICS, PROGRAMMING, THEORY_OF_PROBABILITY
}

class Student
{

}

class SubjectStudent
{
    private Student student;
    private Subject subject;
    private Number mark;
    public SubjectStudent(Student student, Subject subject) {
        this.student = student;
        this.subject = subject;
    }
    public SubjectStudent(Student student, Subject subject, Number mark) {
        this.student = student;
        this.subject = subject;
        setMark(mark);
    }
    void setMark(Number mark) {
        if ((subject==Subject.MATHEMATICAL_ANALYSIS) || (subject==Subject.PHYSICS))
            this.mark=Math.round(mark.doubleValue());
        else
            this.mark=mark.doubleValue();
    }
    public Number getMark() {
        return mark;
    }
    public Student getStudent() {
        return student;
    }
    public Subject getSubject() {
        return subject;
    }
    public boolean isSubject(Subject subject)
    {
        return this.subject.equals(subject);
    }
    public boolean isStudent(Student student)
    {
        return this.student.equals(student);
    }
}
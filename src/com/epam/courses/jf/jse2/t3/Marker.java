package com.epam.courses.jf.jse2.t3;

public class Marker extends AbstractWriter implements Stationery
{
    public Marker() {
        super(200);
    }
    @Override
    public double getPrice() {
        return 2;
    }
}

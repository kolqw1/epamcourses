package com.epam.courses.jf.jse2.t3;

import com.epam.courses.jf.jse2.t2.*;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class StationeryTest {
    @Test
    public void testTotalCost()
    {
        ArrayList<Stationery> mass=new ArrayList<>();
        mass.add(new Note());
        mass.add(new Note());
        mass.add(new Pen());
        mass.add(new Pencil());
        int totalCost=0;
        for (Stationery i:mass) {
            totalCost+=i.getPrice();
        }
        assertEquals(13,totalCost);
    }
    @Test
    public void testWriteRead()
    {
        Note note=new Note();
        Pen pen=new Pen();
        assertTrue(pen.write(note, "qwerty"));
        assertTrue(pen.write(note, "qwerty"));
        assertFalse(pen.write(note, "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"));
        assertEquals("qwertyqwerty", note.read());
    }
}
package com.epam.courses.jf.jse2.t3;

public class Paper extends AbstractWritable implements Stationery
{
    public Paper(){
        super(100);
    }
    @Override
    public double getPrice() {
        return 4;
    }
}

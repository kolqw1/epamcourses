package com.epam.courses.jf.jse2.t3;

public class Note extends AbstractWritable implements Stationery
{
    public Note(){
        super(200);
    }
    @Override
    public double getPrice() {
        return 5;
    }
}

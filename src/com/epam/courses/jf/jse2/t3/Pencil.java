package com.epam.courses.jf.jse2.t3;

public class Pencil extends AbstractWriter implements Stationery
{
    public Pencil() {
        super(150);
    }
    @Override
    public double getPrice() {
        return 2;
    }
}

package com.epam.courses.jf.jse2.t3;

public interface Stationery
{
    double getPrice();
}

interface Writer
{
    boolean write(Writable writable, String str);
}

interface Writable
{
    boolean write(String str);
    String read();
}

abstract class AbstractWritable implements Writable
{
    private StringBuilder str=new StringBuilder();
    private int freeSpace;
    AbstractWritable(int freeSpace){
        this.freeSpace=freeSpace;
    }
    public int getFreeSpace() {
        return freeSpace;
    }
    @Override
    public boolean write(String str)
    {
        if (freeSpace-str.length()>=0) {
            this.str.append(str);
            freeSpace-= str.length();
            return true;
        }
        return false;
    }
    @Override
    public String read()
    {
        return str.toString();
    }
}

abstract class AbstractWriter implements Writer
{
    private int canWriteSymbols;
    AbstractWriter(int canWriteSymbols)
    {
        this.canWriteSymbols=canWriteSymbols;
    }
    @Override
    public boolean write(Writable writable, String str) {
        if (canWriteSymbols-str.length()>=0)
        {
            if (writable.write(str))
            {
                canWriteSymbols -= str.length();
                return true;
            }
        }
        return false;
    }
}


package com.epam.courses.jf.jse2.t3;

public class Pen extends AbstractWriter implements Stationery
{
    public Pen()
    {
        super(100);
    }
    @Override
    public double getPrice() {
        return 1;
    }
}

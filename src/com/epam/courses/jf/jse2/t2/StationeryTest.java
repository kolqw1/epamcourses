package com.epam.courses.jf.jse2.t2;

import org.junit.Test;

import static org.junit.Assert.*;

public class StationeryTest {

    @Test
    public void testTotalCost() throws Exception {
        Stationery stationery=new Stationery();
        stationery.setMarker(1);
        stationery.setPaper(2);
        assertEquals(6, stationery.totalCost(), 0);
    }
}
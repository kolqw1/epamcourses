package com.epam.courses.jf.jse2.t2;

public class Stationery {
    final static double paperCost=1;
    final static double pencilCost=2;
    final static double penCost=3;
    final static double markerCost=4;
    final static double paperClipCost=5;
    final static double punchedPocketCost=6;
    private int paper;
    private int pencil;
    private int pen;
    private int marker;
    private int paperClip;
    private int punchedPocket;
    public int getPaper() {
        return paper;
    }
    public void setPaper(int paper) {
        this.paper = paper;
    }
    public int getPencil() {
        return pencil;
    }
    public void setPencil(int pencil) {
        this.pencil = pencil;
    }
    public int getPen() {
        return pen;
    }
    public void setPen(int pen) {
        this.pen = pen;
    }
    public int getMarker() {
        return marker;
    }
    public void setMarker(int marker) {
        this.marker = marker;
    }
    public int getPaperClip() {
        return paperClip;
    }
    public void setPaperClip(int paperClip) {
        this.paperClip = paperClip;
    }
    public int getPunchedPocket() {
        return punchedPocket;
    }
    public void setPunchedPocket(int punchedPocket) {
        this.punchedPocket = punchedPocket;
    }
    public double totalCost()
    {
        return paper*paperCost+pencil*pencilCost+pen*penCost+marker*markerCost+paperClip*paperClipCost+punchedPocket*punchedPocketCost;
    }
}

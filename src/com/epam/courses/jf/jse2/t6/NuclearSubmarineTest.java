package com.epam.courses.jf.jse2.t6;

import org.junit.Test;

import static org.junit.Assert.*;


public class NuclearSubmarineTest {

    @Test
    public void testSail() throws Exception {
        NuclearSubmarine nuclearSubmarine=new NuclearSubmarine();
        nuclearSubmarine.sail();
    }
    @Test
    public void testAnnotation()  throws Exception {
        NuclearSubmarine nuclearSubmarine=new NuclearSubmarine();
        System.out.println(nuclearSubmarine.getClass().getAnnotations()[0]);
        System.out.println(nuclearSubmarine.getClass().getMethod("sail").getAnnotations()[0]);
        System.out.println(nuclearSubmarine.nuclearSubmarineEngine.getClass().getAnnotations()[0]);
        System.out.println(nuclearSubmarine.nuclearSubmarineEngine.getClass().getMethod("start").getAnnotations()[0]);
    }
}
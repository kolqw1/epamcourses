package com.epam.courses.jf.jse2.t6;

@MyAnnotation(name="Nuclear submarine")
public class NuclearSubmarine {
    NuclearSubmarineEngine nuclearSubmarineEngine=new NuclearSubmarineEngine();
    @MyAnnotation(name="Sailing")
    public void sail()
    {
        nuclearSubmarineEngine.start();
        System.out.println("Sailing");
    }
    @MyAnnotation(name="Engine for nuclear submarine")
    class NuclearSubmarineEngine{
        @MyAnnotation(name="Engine started")
        public void start()
        {
            System.out.println("Engine started");
        }
    }
}

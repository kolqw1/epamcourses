package com.epam.courses.jf.jse1;

import java.util.Scanner;

/**
 * Created by Kolya on 28.02.2016.
 */
public class Second {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double eps;
        if (in.hasNextDouble())
            eps=in.nextDouble();
        else{
            System.out.println("Input error!");
            return;
        }
        int n;
        if (eps>0.25)
            n=1;
        else if (eps>0)
            n=(int)Math.floor(Math.sqrt(1/eps));
        else {
            System.out.println("All elements a greater than " + eps);
            return;
        }
        System.out.println(n);
        for (int i=1; i<=n; i++)
            System.out.print(1/Math.pow((1+i), 2) + " ");
    }
}

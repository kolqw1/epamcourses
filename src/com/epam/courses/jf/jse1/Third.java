package com.epam.courses.jf.jse1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Kolya on 28.02.2016.
 */
public class Third {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a, b, h;
        try {
            a = in.nextDouble();
            b = in.nextDouble();
            h = in.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Input error!");
            return;
        }
        for (double i=a; i<=b; i+=h)
        {
            System.out.println(i+" "+(Math.tan(2*i)-3));
        }
    }
}

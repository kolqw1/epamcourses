package com.epam.courses.jf.jse1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Kolya on 28.02.2016.
 * Массив может быть нечетной длины. В этом случае центральный элемент будет складываться сам с собой
 */
public class Fourth {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double mass[];
        int len;
        try {
            len = in.nextInt();
            if (len<1)
            {
                System.out.println("Input error!");
                return;
            }
            mass=new double[len];
            for (int i=0; i<len; i++)
                mass[i] = in.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Input error!");
            return;
        }
        double max=Double.NEGATIVE_INFINITY;
        for (int i=0; i<(len+1)/2; i++)
        {
            double tmp=mass[i]+mass[len-i-1];
            if (max<tmp)
                max=tmp;
        }
        System.out.println(max);
    }
}

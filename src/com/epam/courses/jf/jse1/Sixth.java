package com.epam.courses.jf.jse1;
import java.util.ArrayList;
/**
 * Created by Kolya on 28.02.2016.
 */
class Record {
    private String record;
    public void setRecord(String str) {
        record = str;
    }
    public String getRecord() {
        return record;
    }
}

class Notepad
{
    private ArrayList<Record> records=new ArrayList<>();
    /**Печать записей*/
    public void printRecords()
    {
        for (int i=0; i<records.size(); i++)
            System.out.println(records.get(i).getRecord());
    }
    /**
     * Редактирование записи
     * @param num Номер редактируемой записи
     * @param str Новое значение записи
     **/
    public void setRecord(int num, String str)
    {
        Record record=new Record();
        record.setRecord(str);
        records.set(num, record);
    }
    /**
     * Добавление записи в конец массива записей
     * @param str Значение новой записи
     */
    public void addRecord(String str)
    {
        Record record=new Record();
        record.setRecord(str);
        records.add(record);
    }
    /**
     * Удаление записи
     * @param num Номер удаляемой записи
     */
    public void delRecord(int num)
    {
        records.remove(num);
    }
}

public class Sixth {
    public static void main(String[] args) {
        Notepad notepad=new Notepad();
        notepad.addRecord("AAA");
        notepad.addRecord("BBB");
        notepad.addRecord("CCC");
        notepad.addRecord("DDD");
        notepad.delRecord(1);
        notepad.setRecord(1, "EEE");
        notepad.printRecords();
    }
}

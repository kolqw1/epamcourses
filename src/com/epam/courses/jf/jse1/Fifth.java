package com.epam.courses.jf.jse1;

import java.util.Scanner;

/**
 * Created by Kolya on 28.02.2016.
 */
public class Fifth {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len;
        int mass[][];
        if (in.hasNextInt())
        {
            len = in.nextInt();
            if (len<0)
                len=0;
            mass=new int[len][len];
            for (int i=0; i<len; i++)
            {
                mass[i][i]=1;
                mass[i][len-i-1]=1;
            }
            for (int i=0; i<len; i++)
            {
                for (int j=0; j<len; j++)
                {
                    System.out.print(mass[i][j]+" ");
                }
                System.out.println();
            }
        } else {
            System.out.println("Input error!");
        }
    }
}
